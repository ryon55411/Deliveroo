﻿namespace Deliveroo.GameData;

public enum RewardSubCategory : int
{
    Unknown = 0,
    Materiel = 1,
    Weapons = 2,
    Armor = 3,
    Materials = 4,
}
