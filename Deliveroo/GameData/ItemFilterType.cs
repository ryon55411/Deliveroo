﻿namespace Deliveroo.GameData;

public enum ItemFilterType
{
    ShowAllItems = 0,
    HideGearSetItems = 1,
    HideArmouryChestItems = 2,
}
